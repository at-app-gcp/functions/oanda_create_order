const axios = require("axios");
const admin = require("firebase-admin");
const serviceAccount = require("./at202xcloud-e8a6266f9a57.json");
admin.initializeApp({ credential: admin.credential.cert(serviceAccount) });
const db = admin.firestore();

// const body =
// {
//   "order": {
//     "units": "-100",
//     "instrument": "EUR_USD",
//     "timeInForce": "FOK",
//     "type": "MARKET",
//     "positionFill": "DEFAULT"
//   }
// }


const body = {
  order: {
    units: "420",
    stopLossOnFill: {
      timeInForce: "GTC",
      price: "1.2280",
    },
    takeProfitOnFill: {
      price: "1.2325",
    },
    timeInForce: "FOK",
    instrument: "EUR_USD",
    type: "MARKET",
    positionFill: "DEFAULT",
  },
};

const options = {
  method: "post",
  headers: {
    "Content-Type": "application/json",
    Authorization:
      "Bearer 1d5a4136d5279b04d815c7f736b50ea1-60e8d9e1d1ff67ba601219754891ff5f",
  },
};

axios
  .post(
    "https://api-fxpractice.oanda.com/v3/accounts/101-004-10015616-001/orders",
    body,
    options
  )

  .then(function (response) {
    // handle success
    console.log(response.data.orderFillTransaction.id);
    // const dataRef = db
    //         .collection("GLOBAL")
    //         .doc("ACCOUNT")
    //         .set(response.data.account)
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  });
